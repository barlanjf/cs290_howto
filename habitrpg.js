var userID = 'af30a792-111e-4c48-8cd8-a50a0a155f65';
var userToken = '5d594c1c-35e4-4879-8e0b-56bf5308c721'


var baseURL = 'https://habitrpg.com/api/v2/';
var userObjectURL = baseURL +'user/';
var contentURL = baseURL + 'content/';
var statusURL = baseURL + 'status/';
var userTaskURL = baseURL + 'user/tasks/';
var taskID = '82d9b672-5ab3-4f1b-9d47-b9ac62d6aa56'


var contentCategories = [];

function isServerUp() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("GET", statusURL);

        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send();   
    }
   
}


function getContent() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("GET", contentURL);

        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    //console.log(responseBody);
                    //parseContentObject(responseBody);
                    checkFirstItem(responseBody);
                }
        }

        httpREQ.send();   
    }
}


function getUserTasks() {
    
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("GET", userTaskURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);

        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send();   
    }
    
}


function parseContentObject(contentObj) {
    
    for (var item in contentObj) {
        console.log(item);
        contentCategories.push(item);
    }
                                 
}

function checkFirstItem(contentObj) {
    var mysteryCategory = contentObj.todos;
    console.log(mysteryCategory);
}


function getUserObject() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("GET", userObjectURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);

        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send();   
    }

}


function addTask() {
    var taskObject = JSON.stringify({
        text: "I want to be a real boy!", //description
        priority: 2,                      //easy(1), medium(1.5), hard(2)
        tags: {},                         //use tags
        notes: "test",
        down: true,                       //minus poinst allowed?
        up: false,                        //plus points allowed?
        type: "habit"                     //where to put it
    });
    
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("POST", userTaskURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send(taskObject);   
    }

}

function addTask() {
    var taskObject = JSON.stringify({
        text: "I want to be a real boy!", //description
        priority: 2,                      //easy(1), medium(1.5), hard(2)
        tags: {},                         //use tags
        notes: "test",
        down: true,                       //minus poinst allowed?
        up: false,                        //plus points allowed?
        type: "habit"                     //where to put it
    });
    
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("POST", userTaskURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send(taskObject);   
    }

}

function updateTask(taskID) {
    var taskObject = JSON.stringify({
        priority: 1,                      //easy(1), medium(1.5), hard(2)
        notes: "updated after adding!",
        down: false,                       //minus poinst allowed?
        up: true,                        //plus points allowed?
    });
    
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("PUT", userTaskURL + taskID);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send(taskObject);   
    }

}


function deleteTask(taskID) {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
    
    if(httpREQ) {
        httpREQ.open("DELETE", userTaskURL + taskID);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }

        httpREQ.send();   
    }
}




window.onload = function() {
  // isServerUp();
  //  getContent();
  //  getUserTasks();
  //  getUserObject();
  //  addTask();
    //updateTask(taskID);
    deleteTask(taskID);

}

