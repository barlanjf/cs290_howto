<h2>Setting Up</h2>
<h3>Getting the User ID and API Token for development</h3>
<p>To develop for HabitRPG, we first need to get our <code class="accent">UID</code> and API <code class="accent">token</code>. These are necessary information to properly communicate with the API. Without them, we will not be able to get a response from our request.</p>
 
<p>1 - Login/Register if you have yet to do so. This can be accessed by clicking the <code class="accent">Play HabitRPG</code> button in the upper right corner of the home page, which can be accessed here: <a href="https://habitrpg.com/">HabitRPG</a> </p>
<img class="centerImg" src="images/01/login%20button.PNG"> </br></br>


<p>2 - Go ahead and Login if you already have a membership, or register if not.</p>
<img class="centerImg" src="images/01/Login_Register.PNG"></br></br>


<p>3 - After logging in, you will have a menu bar at the top of the screen. In the upper right corner is the setting button, represented by a cogwheel. You will want to click it and select the <code class="accent">API</code> link from the resulting pop-up menu.</p>
<img class="centerImg" src="images/01/settings.PNG"></br></br>

<p>4 - After clicking the <code class="accent">API</code> link, you will arrive at a screen showing your <code class="accent">User ID</code>, <code class="accent">API Token</code>, <code class="accent">QR code</code> and an area to add <code class="accent">Webhooks</code>. Go ahead and take note of your <code class="accent">User ID</code> and <code class="accent">API token</code>. It is important to mention that you should never share your API Token - think of this like a password to your account! You also do not want to share the QR Code generated as it also contains the API Token. </p>

<img class="centerImg" src="images/01/API_image.PNG"></br></br>

<a href='index.php?page=2'>Next: Server Status</a>