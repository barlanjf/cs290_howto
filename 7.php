<h2>User Task</h2>
<h3>Deleting a specific task</h3>
Now that we've learned how to retrieve, add and update tasks from the server it's time to learn to destroy them! </br></br>

Deleting task is actually the simplest of all the methods we've learned so far. For this we just need to have the <code class="accent">taskID</code> associated with the task item we are deleting. We must send the request via <code class="accent">DELETE</code>.</br></br>


<code class="codeExample">Code:</code> </br>
<pre>
var baseURL = 'https://habitrpg.com/api/v2/';
var userTaskURL = baseURL + 'user/tasks/';
var taskID = '82d9b672-5ab3-4f1b-9d47-b9ac62d6aa56';

function deleteTask(taskID) {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("DELETE", userTaskURL + taskID);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
       
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send();  
    }
}

</pre>

</br></br>

<code class="codeResponse">Response:</code></br></br>
If the deletion is successful, we will get a an empty object in response. </br>
<img src="images/02/image15.png"></br></br>

<a href='index.php?page=8'>Next: More User Info</a>