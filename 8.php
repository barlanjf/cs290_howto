<h2>User Task</h2>
<h3>Getting more detailed information about the user</h3>

Now that you understand the schema for the objects returned by our request, let’s move on to the big cheese! There is one object that we can request from the API server that will return to us all the information about the user, the <code class="accent">user object</code>. The <code class="accent">user object</code> contains all the tasks, items, gears, guild and party, current challenges etc... associated with a user account. This will probably the object you will want to retrieve if you want to get a more detailed information about the user than just their current tasks. For example, if we wanted to know about the user's avatar hp, mp, experience points, current level, gears and inventory, we would need to pull this object.</br></br>

So let’s go ahead and retrieve it! Once again, you will need to use provide the <code class="accent">UserID</code> and <code class="accent">token</code>.</br></br>

<code class="codeExample">Code:</code>
<pre>
var baseURL = 'https://habitrpg.com/api/v2/';
var userObjectURL = baseURL +'user';

function getUserObject() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("GET", userObjectURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send();  
    }
}
</pre>
</br></br>

<code class="codeResponse">Response:</code></br></br>
What we get is an object that contains other objects and arrays.</br>
<img src="images/02/image02.png"></br></br>

So what’s inside here? Well as you can see, we can also access the task list from part 1 by accessing dailys, habits, and todos arrays! So this is another way to get those task list, however, note that this object can get pretty large if there is a lot of information on the user, so if memory or bandwidth is an issue, and you only care about tasks, you should get the task list using the previous method instead of retrieving it from the user object. </br></br>

Unfortunately, HabitRPG does not have any alternative way to retrieve a lot of the information without retrieving the user object. Outside of tasks list, everything detail associated with a user must be retrieved from the user object. </br></br>

Some important things to note:</br></br>
<code class="accent">id</code> is the userID of the user.</br></br>

<code class="accent">auth</code> object contains email, hashed_password and username. You probably don’t want to share this.</br></br>

<code class="accent">inbox</code> contains messages.</br></br>

<code class="accent">invitation</code> contains the invites from guilds</br></br>

<code class="accent">items</code> contains the various items that a user owns, as well as containing information about the last item drop.</br></br>

<code class="accent">preference</code> contains some user setup information. What you want to pay attnetion here is the ‘hair’ object, ‘size’, ‘shirt’ and ‘skin’ - with these elements, you can recreate the user’s avatar provided you have the images for each of those category.</br></br>

<code class="accent">profile</code> contains the name associated with the user’s avatar.</br></br>

<code class="accent">stats</code> contains info about the user’s avatar. In this object, you have the current buffs</br></br>

As mentioned earlier, what if wanted to know about the user's gears? Well we just look at the item object! Let’s take a look at how it is structured:</br></br>

<img src="images/02/image03.png"><img src="images/02/image21.png"></br></br>

If you’re at all familiar with any rpg game, then you know what these are - stats! As you can see, you have a ton of information here about the user’s avatar. </br></br>

Hopefully you've learned to use HabitRPG with JavaScript! With the knowledge you've gained here, you can see how it would be useful in creating third party apps/extensions that can interact with HabitRPG. There are a ton more stuff you can do with the API, like retrieving guild and party information, quests, challenges, etc... so make sure to check the API documentation for a list of other things you can do! </br></br>

<img src="images/presskit/vice_reborn_by_baconsaur.png">