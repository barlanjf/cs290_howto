<h2>User Task</h2>
<h3>Retrieving All of a user’s task list</h3>


In HabitRPG, each user has a set of tasks that they have to do. These are <code class="accent">Habits</code>, <code class="accent">Dailies</code> and <code class="accent">To-Dos</code>. In the web based screen, you might see them like this.  </br></br>


<img src="images/02/image12.png"> </br></br>

All of these information can be retrieved by a single request to the user tasks. However, unlike our other previous request, we must now include a header with our GET request. This must include your UserID set to ‘x-api-user’ and the Token set to ‘x-api-key’. In the code below, I’ve stored the user ID and the token id in variables userID and userToken.  </br></br>

<code class="codeExample">Code:</code>
<pre>

var baseURL = 'https://habitrpg.com/api/v2/';
var userTaskURL = baseURL + 'user/tasks/';

function getUserTasks() {
   
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("GET", userTaskURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send();  
    }
   
}
</pre>

<code class="codeResponse">Response:</code></br></br>
You should get an array of objects. Each object can be accessed via the bracket notation of the array (ex, array[0] refers to the first object).</br></br>
<img src="images/02/image08.png"> </br></br>


Each of these object in the array represent a task (remember that all the item in the Habit, Dailies and To Dos are all considered tasks). So for the given example picture above, the first object is the item <code class="accent">smoke</code> under <code class="accent">Habit</code>. You can check what kind of object it is by accessing <code class="accent">object.type</code> (returns ‘habit’,  ‘daily’, ‘todo’).</br></br>

If you look look closer to each object’s content, you will see that objects belonging to Habit contains 13 items, objects belonging to Dailies contains 16 items, and objects belonging to To-Dos have 14 items. The structure of each are such:</br></br>


Objects in Habit:
<pre class="schema">
{
    "text": "Smoke",
    "challenge": {},
    "attribute": "str",
    "priority": 2,
    "value": 0,
    "tags": {}
    "notes": "Sample Bad Habits: - Smoke - Procrastinate",
    "dateCreated": "2015-05-22T04:34:46.583Z",
    "id": "87bee964-3385-4556-a9db-78d8c45c820d",
    "down": true,
    "up": false,
    "history": [],
    "type": "habit"
  }
</pre>


Objects in Dailies:
<pre class="schema">
  {
    "text": "Eat Breakfast",
    "challenge": {},
    "attribute": "str",
    "priority": 1,
    "value": 1,
    "tags": {
      "a62084e9-40f9-4fe1-94bb-bdf338b66e3e": false
    },
    "notes": "",
    "dateCreated": "2015-05-22T04:39:40.418Z",
    "id": "195c4aec-0893-4460-96ae-509438fa843f",
    "streak": 1,
    "checklist": [],
    "collapseChecklist": false,
    "repeat": {
      "su": true,
      "s": true,
      "f": true,
      "th": true,
      "w": true,
      "t": true,
      "m": true
    },
    "completed": true,
    "history": [],
    "type": "daily"
  }
</pre>


Objects in To-Dos:
<pre class="schema">
  {
    "text": "cs290 - Turn in How-To Guide",
    "date": "2015-05-24T04:00:00.000Z",
    "challenge": {},
    "attribute": "str",
    "priority": 2,
    "value": 0,
    "tags": {},
    "notes": "",
    "dateCreated": "2015-05-23T02:33:47.906Z",
    "id": "365f9f69-d246-4e2e-b70c-8cd63131e40b",
    "checklist": [],
    "collapseChecklist": false,
    "completed": false,
    "type": "todo"
  }
</pre>

</br></br>

Most of the attributes of the objects are self-explanatory, but there are a few that you may want to take note of: </br></br>

<code class="accent">id</code> is the unique id generated for the task. You will want to take note of this as this is how you will access specific task, or request it specifically from the server (more on this later).</br></br>

<code class="accent">priority</code> is actually the difficult of the task that you’ve set. This is available in objects inside 'Habit' and 'To-Dos' only.</br></br>

1 = easy  | 1.5 = Medium | 2 = Hard</br>
<img src="images/02/image23.png"></br></br>


<code class="accent">streak</code> is the number of times a user has been doing the task without skipping for a day. This is available only in objects under Dailies.</br>
<img src="images/02/image19.png"></br></br>

<a href='index.php?page=5'>Next: Adding Tasks</a>

