<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
        
        
    </head>

    <body>
        <div id="topBar">
                <h5>How-To: Using HabitRPG API with JS | Author: Joseph Barlan</h5>
        </div>
        
        <div id="navContainer">
            <div id="navContent">
                <img class="logoImg" src="images/presskit/habitrpg_pixel.png">
                <?php  include 'navigation.php';?>
            </div>
        </div>
        
        <div id="leftContainer">
            <div id="leftHeader">

            </div>

            <div id="leftContent">
                <?php include 'pageLoader.php' ?>
            </div> 
        </div>

        
    </body>
    
    
</html>

<?php
?>