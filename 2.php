<h2>Server Status</h2>
<h3>Checking the status of the server</h3>

Before you attempt to retrieve any information or make changes to user accounts, it is best that you check the server status first. Make sure it is up and running, and if not, then appropriately handle it. </br> </br>
    
So how do you do this? It’s pretty simple. We just have to perform a <code class="accent">GET</code> request to the status URL. </br></br>


<code class="codeExample">Code: </code>
<pre> 
var baseURL = 'https://habitrpg.com/api/v2/';
var statusURL = baseURL + 'status';

function isServerUp() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("GET", statusURL);
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send();  
    }
  
}
</pre>
</br></br>

<code class="codeResponse">Response:</code></br></br>

An object with status element containing <code class="accent">up</code> or <code class="accent">down</code>. Obviously, the status we are looking for is <code class="accent">up</code>.</br></br>

<img src="images/02/image11.png"></br></br>


<a href='index.php?page=3'>Next: Content Keys</a>