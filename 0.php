<img class="introImg" src="images/presskit/HabitRPGPromoThin.png">

<h2>Introduction </h2>

This is a tutorial on using the <code class="accent">HabitRPG API</code>. In this tutorial we will be covering on how to retrieve user’s character information (such as tasks, to-do list, items, equipments, etc..) using <code class="accent">JavaScript</code>.  </br></br>

Specifically, this tutorial will show you how to retrieve user's tasks list, update the task list, delete tasks and retrieve user detailed information. We will break down how to send request to the API server, and determine the schema of the <code class="accent">JSON objects</code> that are returned to us. By learning the schema of each object, you will understand how to retrieve specific information/data about the user and their avatar from HabitRPG. </br></br>

If you'd like to access the full <code class="accent">API documentation</code>, it is located here: <a href='https://habitrpg.com/static/api'>https://habitrpg.com/static/api</a> </br></br>


<a href='index.php?page=1'>Next: Setting Up</a>