<h2>Content Keys</h2>
<h3>Part 1 - Accessing the Encyclopedia of HabitRPG</h3>

Now that you know that the server is up, you can start looking up for user information. But before we do that, we should first load all available content objects available in HabitRPG. Think of this as the encyclopedia of the HabitRPG world, and it contains information for every objects (from items to monster names and stats). </br></br>

Why do we need to load this first? Almost all objects we will be retrieving from HabitRPG depends on item <code class="accent">keys</code>. This is important if you want to display pertinent information given just a key. For example, later in this tutorial we will be getting the equipped gears of the user such as their weapon. When we retrieve it, we will receive a simple key such as ‘weapon_warrior_0’ as the response. This doesn’t tell us much, like what is the name of this weapon? what about the stats? These are information that we can look up from the ‘encyclopedia’ (content object) by simply looking up the key!</br></br>

<img class="centerImg" src="images/presskit/Equipment%20Sample%20Screen.png"> </br>

So how do we get this all encompassing encyclopedia? Surprisingly (probably not), this is no different from checking if the server is up. All we need to do is change the URL appropriately. We will still be using <code class="accent">GET</code> to send our request. </br></br>


<code class="codeExample">Request Code:</code>
<pre>
var baseURL = 'https://habitrpg.com/api/v2/';
var contentURL = baseURL + 'content';

function getContent() {
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("GET", contentURL);
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send();  
    }
}
</pre>

<code class="codeResponse">Response:</code> <br></br>
An object containing more objects and arrays.</br>
<img src="images/02/image20.png"></br></br>

This response object we get back is actually sorted by category. So let’s take a look at what these things contain. You'll need to familiarize yourself with them for reference if you want to properly get information related to items about the user. </br> </br>

First let’s break out this returned object and see what the categories are. We will do this by changing the <code class="accent">console.log(responseBody)</code> in the previous code to call a function that we will write to iterate over the object and print each category. </br> </br>


<code class="codeExample">Code:</code>
<pre>
function parseContentObject(contentObj) {
    for (var item in contentObj) {
        console.log(item);
    }                      
}
</pre>


<code class="codeResponse">Response: </code></br></br>
We get an object container multiple objects and arrays corresponding to categories of the habitRPG world! </br></br>
<img src="images/02/image06.png"> </br></br>


As you can see there are 22 objects here. Notice that those with curly brackets mean that it is an object, and square brackets mean it is an array. These categories and the items that are contained within them make up the world of HabitRPG. </br></br>



<h3>Part 2 - Accesing the keys from the enclopedia</h3>

To make the content object we just pulled useful, we need to know how to access the keys associated for the items in these categories. </br></br>

So how do we access each category of the object? We use the <code class="accent">dot operator</code>. Let’s say we wanted to see what’s inside the object <code class="accent">mystery</code>.</br></br>


<code class="codeExample">Code example:</code>
<pre>
function checkFirstItem(contentObj) {
    var mysteryCategory = contentObj.mystery;
    console.log(mysteryCategory);
}
</pre>


<code class="codeResponse">Response:</code> </br> </br>
We get an object of more object for this one.  </br></br>
<img src="images/02/image13.png"></br></br>

So what’s inside these? Let’s take a look further. </br>
<img src="images/02/image14.png"></br></br>


Item Sets! As you can see mystery category contains item sets. The first object with key 201402 contains 3 items belonging to the 'Winged Messenger Set', and 201403 contains 2 items belonging to the 'Forest Walker Set'. If you delve deeper into each object, you’ll find that all of them are item sets containing some n number of items. </br></br>

So now that you know how to access the items in each category, you'll want to know what is inside each of them if you are designing a 3rd-party app that required information on specific items. </br></br>

<a href='index.php?page=4'>Next: Retrieving Tasks</a>
