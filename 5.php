<h2>User Task</h2>
<h3>Adding a task to the list</h3>

Now that you know how to retreive tasks and its schema, let’s work on adding a task using the API!

The first thing we need to do is create an object of the task type. If you don’t remember how those objects look like, go back to the previous page.

For example, an object of a task of type ‘habit’ looks like:


object in Habit Column
<pre class="schema">
{
    "text": "Smoke",
    "challenge": {},
    "attribute": "str",
    "priority": 2,
    "value": 0,
    "tags": {}
    "notes": "Sample Bad Habits: - Smoke - Procrastinate",
    "dateCreated": "2015-05-22T04:34:46.583Z",
    "id": "87bee964-3385-4556-a9db-78d8c45c820d",
    "down": true,
    "up": false,
    "history": [],
    "type": "habit"
}
</pre>

We don’t need to fill in all the information as the API will auto fill in a lot of these for us, like date created and id. So let’s go ahead and create a test task object. Remember, we must send a JSON object so we will use a stringify method.

<pre>
    var taskObject = JSON.stringify({
        text: "I want to be a real boy!", //description
        priority: 2,                      //easy(1), medium(1.5), hard(2)
        tags: {},                         //use tags
        notes: "test",
        down: true,                       //minus poinst allowed?
        up: false,                        //plus points allowed?
        type: "habit"                     //where to put it
    });

</pre>

now we just need to send this via <code class="accent">POST</code>:</br></br>

<code class="codeExample">Code:</code>
<pre>
var baseURL = 'https://habitrpg.com/api/v2/';
var userTaskURL = baseURL + 'user/tasks/';

function addTask() {
    var taskObject = JSON.stringify({
        text: "I want to be a real boy!", //description
        priority: 2,                      //easy(1), medium(1.5), hard(2)
        tags: {},                         //use tags
        notes: "test",
        down: true,                       //minus poinst allowed?
        up: false,                        //plus points allowed?
        type: "habit"                     //where to put it
    });
   
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("POST", userTaskURL);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
       
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send(taskObject);  
    }
}
</pre>
</br></br>

<code class="codeResponse">Response:</code></br></br>
After sending our POST, we get a response of the created object, with any other attributes auto-filled in for us by the API </br>
<img src="images/02/image05.png"></br></br>

Here’s the graphical result in the web (Before and After)</br>
<img src="images/02/image09.png"> <img src="images/02/image07.png"> </br></br>

Notice that the task we created is now under ‘Habits’ because we used type ‘habit’. You can of course change this to add to the appropriate categories, and you can also add more information to your task object should you need to add more detail. For To-Do’s for example, you probably want to add information on when it is due by adding a ‘date’ attribute to the object. </br></br>

<a href='index.php?page=6'>Next: Updating Tasks</a>
