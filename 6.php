<h2>User Task</h2>
<h3>Updating a specific task</h3>

Alright! now that we can retrieve and create task, how about updating an already existing task?</br></br>

The first thing we need to do is get the <code class="accent">ID</code> of the task we’re going to update. You can get this either through iterating over the task object we retrieved from part1, or however you can get it. The important thing here is that we must have the <code class="accent">ID</code>. Going back to the example from part2, remember that we also got an object returned to us. Let’s take the id from that and update the task! Here’s the task: </br></br>

Compressed: </br>
<img src="images/02/image10.png"></br></br>

Expanded: </br>
<img src="images/02/image16.png"></br></br>

Object: </br>
<img src="images/02/image05.png"></br></br>

Let’s say we wanted to update the <code class="accent">notes</code>, <code class="accent">direction/action</code>, and <code class="accent">difficulty</code> (see expanded above). To do this, we must create an object with the information we plan to update. </br>
<pre>
    var taskObject = JSON.stringify({
        priority: 1,                      //easy(1), medium(1.5), hard(2)
        notes: "updated after adding!",
        down: false,                       //minus poinst allowed?
        up: true,                        //plus points allowed?
    });
</pre>

</br></br>

Now that the object is built, we need to sent this to the server via <code class="accent">PUT</code>, and we must append the <code class="accent">userTaskURL</code> with the <code class="accent">taskID</code>.</br></br>


<code class="codeExample">Code</code>
<pre>
var baseURL = 'https://habitrpg.com/api/v2/';
var userTaskURL = baseURL + 'user/tasks/';
var taskID = '82d9b672-5ab3-4f1b-9d47-b9ac62d6aa56'

function updateTask(taskID) {
    var taskObject = JSON.stringify({
        priority: 1,                      //easy(1), medium(1.5), hard(2)
        notes: "updated after adding!",
        down: false,                       //minus poinst allowed?
        up: true,                        //plus points allowed?
    });
   
    var httpREQ = new XMLHttpRequest();
    if(!httpREQ) {
        throw "Could not create XMLHttpRequest()";
    }
   
    if(httpREQ) {
        httpREQ.open("PUT", userTaskURL + taskID);
        httpREQ.setRequestHeader("x-api-user", userID);
        httpREQ.setRequestHeader("x-api-key", userToken);
        httpREQ.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
       
        httpREQ.onreadystatechange = function() {
                if(this.readyState === 4 && this.status === 200) {
                    var responseBody = JSON.parse(this.responseText);
                    console.log(responseBody);
                }
        }
        httpREQ.send(taskObject);  
    }
}
</pre>
</br></br>


<code class="codeResponse">Response:</code></br></br>
We get an object returned to us of the updated task. </br>
<img src="images/02/image04.png"></br></br>


Here you can see the details of the result. Notice that the action changed from minus to plus: </br> </br>
Compressed: </br>
<img src="images/02/image00.png"></br></br>
Expanded </br>
<img src="images/02/image22.png"></br></br>

<a href='index.php?page=7'>Next: Deleting Tasks</a>
